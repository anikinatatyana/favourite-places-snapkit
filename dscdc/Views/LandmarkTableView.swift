//
//  PlaceTableViewCell.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import UIKit

class LandmarkTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }


    lazy var imagePlace: UIImageView = {
        let userImage = UIImageView(frame: CGRect(x: 4, y: 4, width: 45, height: 45))
        userImage.contentMode = .scaleAspectFill
        return userImage
    }()


    lazy var nameLabel: UILabel = {
        let lbl = UILabel(frame: CGRect(x: 60, y: 10, width: self.frame.width - 116, height: 30))
        lbl.textAlignment = .left
        return lbl
    }()

    lazy var imageStar: UIImageView = {
        let imageStar = UIImageView(frame: CGRect(x: 215 , y: 10, width: 20, height: 20))

        imageStar.image = UIImage(systemName: "star.fill")
        imageStar.tintColor = UIColor.orange

        return imageStar
    }()

    private func snapKitImageConstraintAutoLoaut() {
        imageStar.snp.makeConstraints { (make) in
            make.right.equalTo(LandmarkTableViewCell().snp.right).offset(-40)
            make.centerY.equalTo(LandmarkTableViewCell().snp.centerY)
        }
    }

    private func snapKitimagePlaceConstraintAutoLoaut() {
        imagePlace.snp.makeConstraints { (make) in
            make.left.equalTo(LandmarkTableViewCell().snp.left).offset(10)
            make.width.equalTo(45)
            make.height.equalTo(45)
            make.top.equalTo(LandmarkTableViewCell().snp.top).offset(5)
            make.bottom.equalTo(LandmarkTableViewCell().snp.bottom).offset(-5)
        }
    }

    private func snapKitLabelConstraintAutoLoaut() {
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imagePlace.snp.left).offset(55)
            make.centerY.equalTo(LandmarkTableViewCell().snp.centerY)

        }
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        addSubview(imagePlace)
        addSubview(nameLabel)
        addSubview(imageStar)

        snapKitImageConstraintAutoLoaut()
        snapKitimagePlaceConstraintAutoLoaut()
        snapKitLabelConstraintAutoLoaut()
    }

}

