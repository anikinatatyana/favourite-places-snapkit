//
//  NameData.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import Foundation

struct NameData: Codable {

    var name: String
    var isFavorite: Bool
    var imageName: String
    var park: String
    var state: String
    var coordinates: Coordinates
}
