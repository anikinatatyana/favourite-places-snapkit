//
//  Cordinates.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import Foundation

struct Coordinates: Codable {

    var longitude: Double
    var latitude: Double
}
