//
//  DataLoad.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import Foundation

public class DataLoader {

    @Published var nameData = [NameData]()

    init() {
        load()
    }

    func load() {

        if let fileLocation = Bundle.main.url(forResource: "landmarkData", withExtension: "json") {

            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([NameData].self, from: data)

                self.nameData = dataFromJson
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
