//
//  ViewController.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import UIKit
import SnapKit

class PlaceViewController: UIViewController {

    var tableView = UITableView()

    var data = DataLoader().nameData
    var favoriteData: [NameData] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setTableView()
        title = "Landmarks"
        navigationController?.navigationBar.prefersLargeTitles = true

        favoritePlace()
    }

    func setTableView() {

        tableView.frame = self.view.frame
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = .white
        view.addSubview(tableView)

        tableView.register(FavoriteLandmarksTableViewCell.self, forCellReuseIdentifier: "FavoriteLandmarksTableViewCell")
        tableView.register(LandmarkTableViewCell.self, forCellReuseIdentifier: "LandmarkTableViewCell")

    }

    private func configure(cell: LandmarkTableViewCell, for indexPath: IndexPath) {
        let namePlace = data[indexPath.row]
        cell.nameLabel.text = namePlace.name
        cell.imagePlace.image = UIImage(named: namePlace.imageName)
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator

        if namePlace.isFavorite == false {
            cell.imageStar.isHidden = true
        } else {
            cell.imageStar.isHidden = false
        }

    }

    private func favoritePlace() {
        for i in data {
            if i.isFavorite == true {
                favoriteData.append(i)
            }
        }

    }
}

 

extension PlaceViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return data.count
        default:
            return 0

    }
}

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0, indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteLandmarksTableViewCell") as! FavoriteLandmarksTableViewCell
            cell.textLabel?.text = "Favorites only"
            cell.delegate = self

            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LandmarkTableViewCell") as! LandmarkTableViewCell
            configure(cell: cell, for: indexPath)

            return cell
        } else {
            return UITableViewCell()
        }

    }
}

extension PlaceViewController: FavoriteLandmarksTableViewCellDelegate {
    func didChangeSwitchState(_: FavoriteLandmarksTableViewCell, isOn: Bool) {
        if isOn {
            data = favoriteData
        } else {
            data = DataLoader().nameData
        }
        tableView.reloadData()
    }
}

