//
//  FavoritePlaceTableViewCell.swift
//  dscdc
//
//  Created by Tatyana Anikina on 25.01.2021.
//

import UIKit
import SnapKit

protocol FavoriteLandmarksTableViewCellDelegate : class {
    func didChangeSwitchState(_: FavoriteLandmarksTableViewCell, isOn: Bool)
}

class FavoriteLandmarksTableViewCell: UITableViewCell {

    weak var delegate: FavoriteLandmarksTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    @objc func switchAction(_ sender: UISwitch) {
            self.delegate?.didChangeSwitchState(self, isOn: switchFavorites.isOn)
    }

    lazy var switchFavorites: UISwitch = {
        let switchFavorites = UISwitch(frame: CGRect(x: 300 , y: 10, width: 20, height: 20))

        return switchFavorites
    }()

    private func swithcConstraint() {
        switchFavorites.snp.makeConstraints { (make) in
            make.right.equalTo(FavoriteLandmarksTableViewCell().snp.right).offset(-40)
            make.centerY.equalTo(FavoriteLandmarksTableViewCell().snp.centerY)
        }
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        addSubview(switchFavorites)
        swithcConstraint()

        switchFavorites.addTarget(self, action: #selector(switchAction), for: UIControl.Event.valueChanged)

    }

}


